﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using TaskAPI.Context;
using TaskAPI.ITaskRepository;
using TaskAPI.Models;

namespace TaskAPI.TaskRepository
{
    public class TaskRepository : ITask
    {
        private readonly TaskApiDbContext _dbContext;

        public TaskRepository(TaskApiDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        [HttpPost]
        public Tasks AddTask(Tasks task)
        {
            task.CreatedAt = DateTime.Now;
            _dbContext.Tasks.Add(task);
            _dbContext.SaveChanges();
            return task;
        }

        [HttpGet]
        public IEnumerable<Tasks> GetAllTasks()
        {
            var tasks = _dbContext.Tasks.ToList();
            return tasks;
        }

        [HttpGet("{id}")]
        public Tasks GetTaskById(int id)
        {
            var task = _dbContext.Tasks.FirstOrDefault(t => t.Id == id);
            return task;
        }

        [HttpPut("{id}")]
        public void EditTask(int id, Tasks updatedTask)
        {
            var task = _dbContext.Tasks.FirstOrDefault(t => t.Id == id);

            task.Name = updatedTask.Name;
            task.CreatedBy = updatedTask.CreatedBy;
            task.Description = updatedTask.Description;

            _dbContext.SaveChanges();
        }

        [HttpDelete("{id}")]
        public void DeleteTask(int id)
        {
            var task = _dbContext.Tasks.FirstOrDefault(t => t.Id == id);
            _dbContext.Tasks.Remove(task);
            _dbContext.SaveChanges();

        }
    }

}
