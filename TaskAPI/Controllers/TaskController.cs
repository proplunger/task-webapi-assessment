﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using TaskAPI.Context;
using TaskAPI.ITaskRepository;
using TaskAPI.Models;

[ApiController]
[Route("api/tasks")]
[Authorize]
public class TasksController : ControllerBase
{
    ITask _repo;

    public TasksController(ITask repo)
    {
        _repo = repo;
    }

    [HttpPost]
    public ActionResult<Tasks> AddTask(Tasks task)
    {
        _repo.AddTask(task);
        return Ok(task);
    }

    [HttpGet]
    public ActionResult<IEnumerable<Tasks>> GetAllTasks()
    {
        var tasks = _repo.GetAllTasks().ToList();
        return Ok(tasks);
    }

    [HttpGet("{id}")]
    public ActionResult<Tasks> GetTaskById(int id)
    {
        var task = _repo.GetTaskById(id);
        if (task == null)
        {
            return NotFound();
        }
        return Ok(task);
    }

    [HttpPut("{id}")]
    public IActionResult EditTask(int id, Tasks updatedTask)
    {
        var task = _repo.GetTaskById(id);
        if (task == null)
        {
            return NotFound();
        }

         _repo.EditTask(id, updatedTask);
         return NoContent();
    }

    [HttpDelete("{id}")]
    public IActionResult DeleteTask(int id)
    {
        var task = _repo.GetTaskById(id);
        if (task == null)
        {
            return NotFound();
        }

        _repo.DeleteTask(id);

        return NoContent();
    }
}
